#include <stdio.h>

int main()
{
  int c, prev_sym=0, coment=0, sym=0, str=0, digraph = 0;
  int one_line_comment = 0;
  int suspect_comment = 0;
  // TODO: add support for one line comments

  while((c=getchar())!=EOF)
  {
    if (digraph)
    {
        putchar(c);
        digraph=0;
        prev_sym=0;
        continue;
    }
    else if (c == '\\')
    {
        putchar(c);
        prev_sym = c;
        digraph = 1;
        continue;
    }

    if (suspect_comment)
    {
        if (c == '*') /*начался многострочный комментарий*/
            coment = 1;
        else if (c == '/')
            one_line_comment = 1;
        else
        {
            putchar('/');
            putchar(c);
        }
        suspect_comment = 0;
        prev_sym = c;
    }
    else if(coment)
    {
      if(prev_sym == '*' && c=='/')
        coment=0;
      prev_sym = c;
    }
    else if (one_line_comment)
    {
        if (c == '\n')
        {
            one_line_comment = 0;
            putchar(c);
        }
        prev_sym = c;
    }
    else if(str)
    {
      putchar(c);
      if(c=='"' && prev_sym !='\\')
        str=0;
      prev_sym=c;
    }
    else if(sym)
    {
      putchar(c);
      if(c=='\'' && prev_sym !='\\')
      {
        sym=0;
      }
      prev_sym=c;
    }
    else
    {
      if(c=='"')
      {
        putchar(c);
        str=1;
      }
      else if(c=='\'')
      {
        putchar(c);
        sym=1;
      }
      else if (c == '/')
      {
        suspect_comment = 1;
      }
      else
      {
        // TODO: find a way how to handle '/': it might be division or
        // start of the comment
        putchar(c);
      }
      prev_sym=c;
    }
  }
}
